
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM


PROGRAM _CYCLIC
	
	(*
		Test sample Logic 1
		Get Button Signal AND THEN Control CountUp/Down 
		Count Up is Orange LED on
	*)

	Count;
	
	IF EDGEPOS(diCountUp) THEN
		diCountUpSet := NOT(diCountUpSet);
	END_IF;
	
	
	IF diCountUpSet THEN
		doCountDownLED	:= FALSE;
		doCountUpLED	:= TRUE;
	ELSE
		doCountDownLED	:= TRUE;
		doCountUpLED	:= FALSE;	
	END_IF;
	
	
	
	(*
		Test sample Logic 2
		heater and Cooler LED On/Off
	*)
	CurrentTemp;
	doHeaterLED 	:= HeaterStatus; 
	doCoolerLED 	:= CoolerStatus;
	
END_PROGRAM


PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

