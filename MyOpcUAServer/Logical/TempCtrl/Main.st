
PROGRAM _INIT
	
	(* 
		Input init value new variables
	 *)
	CurrentTemperature := 25;
	
END_PROGRAM


PROGRAM _CYCLIC

	(*
		Test sample Logic 1
	*)
	IF StartCountUP THEN
		Count := Count + 1;
	ELSE
		Count := Count - 1;
	END_IF;
	
	
	(*
		Test sample Logic 2
	*)
	IF 30 <= CurrentTemperature  THEN
		HeaterOn	:= FALSE;
		CoolerOn	:= TRUE;
	ELSIF 23 <= CurrentTemperature AND CurrentTemperature < 30 THEN
		HeaterOn 	:= FALSE;
		CoolerOn	:= FALSE;
	ELSIF CurrentTemperature < 23 THEN
		HeaterOn 	:= TRUE;
		CoolerOn	:= FALSE;
	END_IF;
	
	
END_PROGRAM


PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

